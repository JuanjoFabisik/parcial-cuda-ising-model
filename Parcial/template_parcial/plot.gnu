set autoscale                        # scale axes automatically
unset log                            # remove any log-scaling
unset label                          # remove any previous labels
set xtic auto                        # set xtics automatically
set ytic auto                        # set ytics automatically
set title "M(t)"
set xlabel "t"
set ylabel "M"
plot "magnetizacion.dat"
