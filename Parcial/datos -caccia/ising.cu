
#include "util_Device.h"
#include "simple_timer.h"


int main(int argc, char **argv)
{

	#if THRUST_DEVICE_SYSTEM == THRUST_DEVICE_SYSTEM_CUDA
	int dev; cudaGetDevice(&dev);
        cudaDeviceProp deviceProp;
        cudaGetDeviceProperties(&deviceProp, dev);
        printf("\nDevice %d: \"%s\"\n", dev, deviceProp.name);
	gpu_timer cronocon;
	gpu_timer cronosin;

	#elif THRUST_DEVICE_SYSTEM == THRUST_DEVICE_SYSTEM_OMP
	printf("OMP version, with %d threads\n", omp_get_max_threads());
	omp_timer cronocon;
	omp_timer cronosin;

	#elif THRUST_DEVICE_SYSTEM == THRUST_DEVICE_SYSTEM_CPP
	printf("CPP version\n");
	cpu_timer cronocon;
	cpu_timer cronosin;
	#endif

	cronocon.tic();


	// primer argumento: tamanio lateral del sistema (default=128)
	int L=(argc>1)?(atoi(argv[1])):(128);
	if(L%2!=0 || L<0){
		std::cout << "error: L debe ser par positivo" << std::endl;
		exit(1);	
	}	
	int N=L*L;

	// segundo argumento: temperatura (default=2.0)
	float temp=(argc>2)?(atof(argv[2])):(2.0);

	// tercer argumento: iteraciones totales (default=100)
	int trun=(argc>3)?(atoi(argv[3])):(100);

	// cuarto argumento: semilla global (default=0)
	int seed = (argc>4)?(atoi(argv[4])):(0);
	srand(seed);


	std::cout << "L="    << L    << std::endl;
	std::cout << "temp=" << temp << std::endl;
	std::cout << "trun=" << trun << std::endl;
	std::cout << "seed=" << seed << std::endl;


/////// DESDE AQUI HACER TODO EN DEVICE ///////

	// declara e inicializamos random la matriz M_{ij} = +-1 
	int *M = alocar_matriz(N);

	// inicializa M_{i,j} = +-1 en forma random
	inicializar_matriz_random(M,N);

	std::ofstream magnetizacion_out("magnetizacion.dat");

	#ifdef MOVIE
	std::ofstream evolucion_out("evolucion.dat");
	#endif

	cronosin.tic();
	// loop temporal
	for(int t=0;t<trun;t++){
		
		#ifdef SCREEN		
		printf("%f \n",t*100.0/trun);
		#endif

		magnetizacion_out << get_magnetizacion(M,N) << std::endl;
	
		// itera solo sobre los sitios pares	
		metropolis(M,temp,L,PARES,t,seed);

		// itera solo sobre los sitios impares
		metropolis(M,temp,L,IMPARES,t,seed);	

		#ifdef MOVIE
		if(t%MOVIE==0 || t==trun-1){
			print_matrix(M,evolucion_out,L);
		}
		#endif

	}

////////////////////////////////////////////////

	float mscon = cronocon.tac();
	float mssin = cronosin.tac();

	printf(" ms_con=%f, ms_sin=%f\n",mscon,mssin);	

	return 0;
}


