#! /bin/bash
#
#$ -cwd
#$ -j y
#$ -S /bin/bash
##  pido la cola gpu.q
#$ -q gpu.q@compute-0-0
##$ -q gpu.q
## pido una placa
#$ -l gpu=1
#
module load cuda-7.5
#ejecuto el binario
#nvprof ./main
#nvprof --print-gpu-trace ./main
#nvprof --print-api-trace ./main
#nvprof --query-events --query-metrics
#nvprof --metrics gld_efficiency,gst_efficiency,branch_efficiency ./main

./ising_cuda 128 2.269 200 42 > out/cuda12.out
./ising_cuda 256 2.269 200 42 > out/cuda22.out
./ising_cuda 512 2.269 200 42 > out/cuda32.out
./ising_cuda 1024 2.269 200 42 > out/cuda42.out
./ising_cuda 2048 2.269 200 42 > out/cuda52.out
./ising_cuda 4096 2.269 200 42 > out/cuda62.out
./ising_cuda 8192 2.269 200 42 > out/cuda72.out
#./main
