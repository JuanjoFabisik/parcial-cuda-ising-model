#include<iostream>
#include<fstream>
#include<cmath>
#include<cstdlib>
#include<thrust/host_vector.h>
#include<thrust/device_vector.h>
#include<thrust/random.h>
#include<thrust/transform.h>
#include<thrust/sequence.h>
#include<thrust/for_each.h>
#include<thrust/iterator/counting_iterator.h>
#include<thrust/iterator/transform_iterator.h>
#include<thrust/iterator/permutation_iterator.h>
#include<iostream>


int * alocar_matriz(int N)
{
    // create a device_ptr 
    thrust::device_ptr<int> M = thrust::device_malloc<int>(N);
    // extract raw pointer from device_ptr
    int * raw_M = thrust::raw_pointer_cast(M);
    return raw_M;
}


// basado en el ejemplo de thrust: https://github.com/thrust/thrust/blob/master/examples/strided_range.cu
// "how to make strided access to a range of values"
template <typename Iterator>
class strided_range
{
    public:

    typedef typename thrust::iterator_difference<Iterator>::type difference_type;

    struct stride_functor : public thrust::unary_function<difference_type,difference_type>
    {
        difference_type shift;
	difference_type to_add;
	difference_type N;

        stride_functor(difference_type shift,difference_type to_add,difference_type N)
            : shift(shift), to_add(to_add), N(N) {}

        __host__ __device__
        difference_type operator()(const difference_type& i) const
        { 
        if (N) { 
	    return (shift + i*2 + to_add)% N; // para armar iteradores arr, aba, izq, der
	}
	else {
	    return shift + i*2 ;              // para armar iteradores par o impar
	}
     }
     };

    typedef typename thrust::counting_iterator<difference_type>                   CountingIterator;
    typedef typename thrust::transform_iterator<stride_functor, CountingIterator> TransformIterator;
    typedef typename thrust::permutation_iterator<Iterator,TransformIterator>     PermutationIterator;

    // type of the strided_range iterator
    typedef PermutationIterator iterator;

    // construct strided_range for the range [first,last)
    strided_range(Iterator first, Iterator last, difference_type shift, difference_type to_add, difference_type N)
        : first(first), last(last), shift(shift), to_add(to_add), N(N) {}
   
    iterator begin(void) const
    {
        return PermutationIterator(first, TransformIterator(CountingIterator(0), stride_functor(shift, to_add,N)));
    }

    iterator end(void) const
    {
        return begin() + ((last - first) + (2 - shift - 1)) / 2;
    }
    
    protected:
    Iterator first;
    Iterator last;
    difference_type shift;
    difference_type to_add;
    difference_type N;
};


struct metropolis_functor
{
    float temp;
    int   seed;

    metropolis_functor(float temp, int seed) : temp(temp), seed(seed) {}

    template <typename Tuple>
    __host__ __device__
    void operator()(Tuple t)
    {

    int   M =  thrust::get<0>(t);
    int izq =  thrust::get<1>(t);
    int der =  thrust::get<2>(t);
    int aba =  thrust::get<3>(t);
    int arr =  thrust::get<4>(t);
    int dis =  thrust::get<5>(t);

    // la magnetizacion total de los vecinos
    int vecinos=izq+der+arr+aba;
		
    // contribucion de nuestro spin sin flipear a la energia  
    int ene0=-M*vecinos;	

    // contribucion a la energia de nuestro spin flipeado
    int ene1=M*vecinos;	

    // metropolis: aceptar flipeo solo si r < exp(-(ene1-ene0)/temp)
    float p=exp(-(ene1-ene0)/temp);

    //thrust random engine
    thrust::default_random_engine randEng(seed);
    thrust::uniform_real_distribution<float> uniDist;
    randEng.discard(dis);
    float r=float(uniDist(randEng));

    if(r<p) thrust::get<0>(t)*=-1;	
    }
};


#define PARES	0
#define IMPARES	1
void metropolis(int *raw_M, float temp, int L, int shift, int t, int seed)
{
    int N=L*L;

    thrust::device_ptr<int> M(raw_M);  

    // iterador para el random engine de thrust
    thrust::counting_iterator<int> discard_iter(t*N);

    typedef thrust::device_vector<int>::iterator Iterator;

    // se arman los iteradores para acceder a M
    strided_range<Iterator> working_M (M, M+N, shift, 0, 0);

    strided_range<Iterator> izq_iter (M, M+N, shift, -1+N, N);

    strided_range<Iterator> der_iter (M, M+N, shift,   +1, N);

    strided_range<Iterator> aba_iter (M, M+N, shift,   +L, N);

    strided_range<Iterator> arr_iter (M, M+N, shift, -L+N, N);


    // basado en ejemplo de thrust: https://github.com/thrust/thrust/blob/master/examples/arbitrary_transformation.cu
    // "how to implement an arbitrary transformation"
    thrust::for_each(thrust::make_zip_iterator(thrust::make_tuple(
                                                                  working_M.begin(),
                                                                  izq_iter.begin(), 
                                                                  der_iter.begin(),
                                                                  aba_iter.begin(),
                                                                  arr_iter.begin(),
                                                                    discard_iter)),
                     thrust::make_zip_iterator(thrust::make_tuple(
                                                                   working_M.end(),
                                                                    izq_iter.end(), 
                                                                    der_iter.end(),
                                                                    aba_iter.end(),
                                                                    arr_iter.end(),
                                                                    discard_iter+N)), //check size
                     metropolis_functor(temp, seed*(shift+1)));

}

// para imprimir una matriz LxL guardada en el HOST
void print_matrix(int *M, std::ofstream &fout, int L)
{
	for(int i=0;i<L;i++){ 
		for(int j=0;j<L;j++){ 
			fout << M[i*L+j] << " "; 
		}
		fout << "\n";
	}
	fout << "\n" << std::endl;
}


// random engine de thrust, para inicializar matriz M
struct GenRand {
    __host__ __device__
    float operator () (int idx) {

    thrust::default_random_engine randEng;
    thrust::uniform_real_distribution<float> uniDist;
    randEng.discard(idx);
    return (uniDist(randEng)>0.5)?(1):(-1);
    }
};


// inicializa la matriz
void inicializar_matriz_random(int *raw_M, int N)
{
    thrust::device_ptr<int> M(raw_M);   	

    thrust::transform(
        thrust::make_counting_iterator(0),
        thrust::make_counting_iterator(N),
        M,          
        GenRand());	
}


// retorna la magnatizacion
float get_magnetizacion(int *raw_M, int N)
{
    thrust::device_ptr<int> M(raw_M);   

    float sum = thrust::reduce(M, M + N);
    return sum*=1.0/N;
}
