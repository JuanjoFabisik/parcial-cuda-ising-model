#include <cstdio>
#include <cstdlib>
#include <cassert>

#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/transform.h>
#include <thrust/reduce.h>
#include <thrust/sequence.h>
#include <thrust/transform_reduce.h>
#include <cusp/array2d.h>
#include <cusp/coo_matrix.h>
#include <cusp/print.h>
using namespace thrust::placeholders;
using namespace std;

////////////////////////////////////////////////////////////////
/*
struct convolution // normaliza por N
{
    public:
    float M;
    float* h;
    float* x;
    convolution(int fSize, float* d_filter_ptr, float* d_x_ptr){
        M = fSize; 
        h = d_filter_ptr;
        x = d_x_ptr;
    };
    __device__
    float operator()(int n0)
    {
        float temp = 0.0;
        for (int i=n0; i<n0+M; i++){
            temp += x[i]*h[i-n0];
        }
        return temp;
    }
};*/

int main(int argc, char *argv[]) 
{
    /*
	int N = 10000;
    int M = 10;
    
    // Allocate memory on host 
	thrust::host_vector<float> h_x(N+M) ;  // Input data 
	thrust::host_vector<float> h_y(N) ;  // Output data /
	thrust::host_vector<float> h_filter(M); // Filter 

	// Setup the arrays 
    thrust::generate(h_x.begin(),h_x.end()-M, rand);
    thrust::copy(h_x.end()-2*M+1,h_x.end()-M, h_x.end()-M+1);
    thrust::generate(h_y.begin(),h_y.end(), rand);
    thrust::generate(h_filter.begin(),h_filter.end(), rand);
        
    thrust::transform(h_x.begin(), h_x.end(), h_x.begin(), _1/RAND_MAX);
    thrust::transform(h_y.begin(), h_y.end(), h_y.begin(), _1/RAND_MAX);
    thrust::transform(h_filter.begin(), h_filter.end(), h_filter.begin(), _1/RAND_MAX);

	// Allocate memory on device 
    thrust::device_vector<float> d_x = h_x;  // Input data 
	thrust::device_vector<float> d_y = h_y;  // Output data 
	thrust::device_vector<float> d_filter = h_filter; // Filter 

    // Punteros crudos en device 
    float* d_x_ptr = thrust::raw_pointer_cast(&d_x[0]);
    float* d_filter_ptr = thrust::raw_pointer_cast(&d_filter[0]);

    // Convolution 
    
    thrust::counting_iterator<int> first(0); // para pasar los indices del output a calcular
    thrust::transform(first, first+N, d_y.begin(), convolution(M, d_filter_ptr, d_x_ptr));
	
	// Print 
    for (int i=0; i<N; i++){
        cout<<"y["<<i<<"]: "<<d_y[i]<<endl;
    } */
    
    
    // 1
    
    cout<<"Pregunta 1 2014"<<endl;
    int A = thrust::reduce(thrust::make_counting_iterator(0), thrust::make_counting_iterator(5));
    cout<<A<<endl;
    
    // 2
    
    cout<<"Pregunta 2 2014"<<endl;
    thrust::device_vector<int> v(10);
    thrust::sequence(v.begin(), v.end());
    cout<<v[0]<<" "<<v[1]<<endl;
    //int *ptr1 = v.begin();
    int *ptr2 = thrust::raw_pointer_cast(v.data());
    int *ptr3 = thrust::raw_pointer_cast(&v[0]);
    //int *ptr4 = &v[0];
    cout<<ptr2<<" "<<ptr3<<" "<<endl;
    cout<<v.data()<<endl;

    cout<<"Pregunta 6 2014"<<endl;
    thrust::host_vector<float>h_x(10);
    thrust::sequence(h_x.begin(), h_x.end(), 0,1);
    cout<<h_x[0]<<" "<<h_x[1]<<" "<<h_x[2]<<" "<<endl;
    
    cout<<"Pregunta 1 2015"<<endl;
    thrust::counting_iterator<int> first(1);
    int N = 3;
    int res = thrust::transform_reduce(first, first+N, _1*(_1+1), 0, thrust::plus<int>());
    cout<<res<<endl;
    
    cout<<"Pregunta 2 2015"<<endl;
    cusp::array2d<int,cusp::host_memory> vc(3,3);
    vc(0,0)=1;
    vc(0,1)=1;
    vc(1,2)=2;
    vc(2,2)=3;
    cusp::print(vc);
    cusp::coo_matrix<int, int, cusp::host_memory> B(vc);
    cout<<B.values[0]<<endl;
    cout<<vc(0,2)<<endl;
    //cout<<B(0,1)<<endl;
    cusp::print(B);
    cout<<B.values[2]<<endl;
    cout<<vc(3,4)<<endl;
    
    cout<<"Pregunta 3 2015"<<endl;
    
}

